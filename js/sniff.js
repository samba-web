/*
    Simple browser sniff
    Copyright (C) 2005 Deryck Hodge <deryck@samba.org>

    Released under the GNU GPL v2 or later.
*/


var ua = navigator.userAgent.toLowerCase();


// Browser
// ***********************************************
var is_opera = ua.indexOf('opera') > -1;
var is_ie = ua.indexOf('msie') > -1 && !is_opera;
var is_ie5 = is_ie && ua.indexOf('msie 5') > -1;
var is_safari = ua.indexOf('safari') > -1;
var is_konq = ua.indexOf('konqueror') > -1;
// Any mozilla browser
var is_moz = ua.indexOf('mozilla') > -1 && !is_ie && !is_opera;
// Just firefox
var is_ff = is_moz && ua.indexOf('firefox') > -1;
// Any gecko-based
var is_gecko = ua.indexOf('gecko') > -1 && !is_konq && !is_safari;
var is_netscape = ua.indexOf('netscape') > -1;
var is_netscape_ie = is_netscape && ua.indexOf('msie') > -1;
var is_netscape_moz = is_netscape && ua.indexOf('gecko') > -1;


// Operating System
// ***********************************************
var is_mac = ua.indexOf('macintosh') > -1 || ua.indexOf('mac') > -1;
var is_linux = ua.indexOf('linux') > -1;
var is_windows = ua.indexOf('windows') > -1;


