#! /usr/bin/python

""" Get headlines froms news.samba.org for inclusion on samba.org """

from urllib import urlopen

html = urlopen('http://news.samba.org/headlines/').read()

headlines = open('/data/httpd/html/samba/news/headlines.html', 'w')
headlines.write(html)
headlines.close()

