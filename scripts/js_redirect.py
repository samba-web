#! /usr/bin/python
#
# js_redirect.py -- setup by Deryck Hodge <deryck@samba.org>
# to run via cron every hour on dp.
#
# This does two things:
# 1) Create the list of options for the select-a-mirror drop-down menu, and 
# 2) generate the Javascript to redirect samba.org to a random US mirror.
#

import os, re

os.chdir('/data/httpd/html/samba')
hosts = open('web_hosts.html', 'r').readlines()
mirrors = []  
us_mirrors = []

for line in hosts:
    if line[:4] == '<li>':
        parts = re.match('<li><a href=(.*/samba/).*">(.*)</a>', line)
        # Make list of mirror_name/url pairs to preserve web_hosts sort. 
        mirrors.append((parts.group(2), parts.group(1)))
        # While we're here, get the US mirrors on their own
        if re.match('<li><a href="(.*/samba/).*">USA (.*)</a>', line) and not line.find('us2') > -1:
            us_mirrors.append(re.match('<li><a href="(.*/samba/).*">USA (.*)</a>', line).group(1))

# Write all mirrors to drop-down menu
menu = open('menu_options.html', 'w')
for m in mirrors:
    menu.write('<option value=' + m[1] + '">' + m[0] + '</option>\n')
menu.close()

us_mirrors.sort
# Write the js for a US mirror redirect
js = open('redirect_include.html', 'w')
js.write('<script language="Javascript" type="text/javascript">\n')
js.write('<!-- Hide from old browsers\n')
js.write('randomMirror = new Array;\n')
for i in range(len(us_mirrors)):
    js.write('randomMirror[' + str(i) + '] = "' + us_mirrors[i] + '"\n')
js.write('\n')
js.write('n = Math.floor(Math.random()*' + str(len(us_mirrors)) + ')\n')
js.write('// end hide -->\n')
js.write('</script>')
js.close()

