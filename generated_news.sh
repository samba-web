#!/bin/bash
#

LC_ALL=C
export LC_ALL
LANG=C
export LANG
LANGUAGE=C
export LANGUAGE

set -u
set -e
umask 0022

generate() {
	local dst="$1"
	local count="$2"
	local filter="$3"

	test "${dst}" -nt "posted_news/" && {
		echo "${dst}: up to date"
		return 0
	}

	echo "${dst}: regenerating"
	files=$(find posted_news/ -type f -regex "${filter}" -printf "%f\n" | sort -r | xargs)
	{
		for f in ${files}; do
			if [[ "$filter" == *snip* ]] && [[ "$f" == *".body.html"* ]] && [[ "$files" == *"${f%.body.html}.snip.html"* ]]; then
				continue
			fi
			echo "<!--#include virtual=\"/samba/posted_news/${f}\" -->"
			count=$((count-1))
			if [[ $count == 0 ]]; then break; fi
		done
	} > ${dst}

	return 0
}

generate_feed() {
	local dst="$1"
	local count="$2"
	local filter="$3"

	test "${dst}" -nt "posted_news/" && {
		echo "${dst}: up to date"
		return 0
	}

	echo "${dst}: regenerating"
	files=$(find posted_news/ -type f -regex "${filter}" -printf "%f\n" | sort -r | xargs)
	{
		echo '<?xml version="1.0" encoding="utf-8"?>'
		echo '<feed xmlns="http://www.w3.org/2005/Atom">'
		echo '<id>https://www.samba.org/samba/news.atom</id>'
		echo '<link href="https://pubsubhubbub.appspot.com/" rel="hub" />'
		echo '<link href="https://www.samba.org/samba/news.atom" rel="self" />'
		echo '<title>Samba - News</title>'
		echo '<updated>'`date +%Y-%m-%dT%H:%m:%SZ`'</updated>'
		echo
		for f in ${files}; do
			if [[ "$filter" == *snip* ]] && [[ "$f" == *".body.html"* ]] && [[ "$files" == *"${f%.body.html}.snip.html"* ]]; then
				continue
			fi
			echo '<entry>'
			MY_UPDATED=`echo $f | sed 's/^\(....\)\(..\)\(..\)-\(..\)\(..\).*/\1-\2-\3T\4:\5:00Z/'`
			echo "<id>https://www.samba.org/samba/news.atom#$MY_UPDATED</id>"
			echo "<link rel='alternate' href='https://www.samba.org/samba/latest_news.html' />"
			echo '<author><name>Samba Team</name><email>webmaster@samba.org</email></author>'
			echo "<updated>$MY_UPDATED</updated>"

			cat posted_news/$f \
				| perl -pe '$/=""; s/<p class=headline>(.*?)<\/p>/<title>$1<\/title>/s' \
				| grep -v '<h5>' \
				| grep -v '<!--' \
				| sed 's:</\?br>:<br />:ig' \
				| sed "/<\/title>/a <content type='xhtml' xml:base='https:\/\/www.samba.org\/samba\/news.atom'><div xmlns='http:\/\/www.w3.org\/1999\/xhtml'>"
			echo '</div></content></entry>'
			count=$((count-1))
			if [[ $count == 0 ]]; then break; fi
		done
		echo '</feed>'
	} > ${dst}.tmp
	mv ${dst}.tmp ${dst}

	return 0
}

generate_latest_stable_release() {
	local dst="$1"
	local download_url="$2"

	pushd history >/dev/null
	ALL_VERSIONS=$(ls samba-*.html | cut -d '-' -f2- | cut -d '.' -f1-3 | sort -t. -k 1,1n -k 2,2n -k 3,3n -u)
	LATEST_VERSION=$(echo "${ALL_VERSIONS}" | tail -1)
	popd >/dev/null

	echo "LATEST_VERSION: ${LATEST_VERSION}"

	local tgz="samba-${LATEST_VERSION}.tar.gz"
	local asc="samba-${LATEST_VERSION}.tar.asc"
	local release_notes="history/samba-${LATEST_VERSION}.html"

	test "${dst}" -nt "${release_notes}" && {
		echo "${dst}: up to date"
		return 0
	}

	echo "${dst}: regenerating"
	{
		echo "<!-- BEGIN: ${dst} -->"
		echo "<p>"
		echo "<a href=\"${download_url}/${tgz}\">Samba ${LATEST_VERSION} (gzipped)</a><br>"
		echo "<a href=\"/samba/${release_notes}\">Release Notes</a> &middot;"
		echo "<a href=\"${download_url}/${asc}\">Signature</a>"
		echo "</p>"
		echo "<!-- END: ${dst} -->"

	} > ${dst}.tmp
	mv ${dst}.tmp ${dst}
}

generate "generated_news/latest_10_headlines.html" "10" ".*\.headline\.html"
generate "generated_news/latest_10_bodies.html" "10" ".*\.body\.html"
generate "generated_news/latest_2_bodies.html" "2" ".*\.\(snip\|body\)\.html"
generate_feed "news.atom" "10" ".*\.\(snip\|body\)\.html"

download_url="https://download.samba.org/pub/samba/stable"
generate_latest_stable_release "generated_news/latest_stable_release.html" "${download_url}"
