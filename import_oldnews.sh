#!/bin/sh
#

LC_ALL=C
export LC_ALL
LANG=C
export LANG
LANGUAGE=C
export LANGUAGE

set -u
set -e
umask 0022

HEADLINES=${1}
BODIES=${2}

LINES="$(cat ${HEADLINES} | grep '<li>')"
NLINES=$(echo "${LINES}" | wc -l)

BODYH5="$(grep --line-number '<h5>' ${BODIES})"

for i in $(seq 1 $NLINES); do
	l=$(echo "${LINES}" | head -${i} | tail -1 | sed -e 's!^[\t ]*!!')
	d=$(echo "${l}" | sed -e 's!^<li>\([^<]*\)<a href=.*!\1!')
	n=$(echo "${l}" | sed -e 's!.*a href="#\([^"]*\)".*!\1!')
	utctime=$(date --date="${d}" --utc +"%Y%m%d-%H%M%S")
	headlinefile="posted_news/${utctime}.${n}.headline.html"
	bodyfile="posted_news/${utctime}.${n}.body.html"
	echo "${i}: ${headlinefile}"
	{
		echo "<!-- BEGIN: ${headlinefile} -->"
		echo "${l}"
		echo "<!-- END: ${headlinefile} -->"
	} > ${headlinefile}

	boundary=$(echo "${BODYH5}" | grep -A1 "<h5><a name=\"${n}\">" | cut -d ':' -f1)
	tmp=$(echo "${boundary}" | wc -l)
	case "${tmp}" in
	2)
		begin=$(echo "${boundary}" | head -1)
		next=$(echo "${boundary}" | tail -1)
		end=$(expr ${next} - 1)
		len=$(expr ${end} - ${begin})
		;;
	1)
		len="10000"
		;;
	*)
		echo "invalid boundary:"
		echo "${boundary}"
		exit 1
		;;
	esac
	echo "${i}: ${bodyfile}"
	{
		echo "<!-- BEGIN: ${bodyfile} -->"
		grep -A${len} "<h5><a name=\"${n}\">" ${BODIES}
		echo "<!-- END: ${bodyfile} -->"
	} > ${bodyfile}
done
