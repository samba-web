#!/bin/bash
#

SAVE_LC_ALL="${LC_ALL}"
SAVE_LANG="${LANG}"
SAVE_LANGUAGE="${LANGUAGE}"

LC_ALL=C
export LC_ALL
LANG=C
export LANG
LANGUAGE=C
export LANGUAGE

set -u
set -e
umask 0022

test -d ".git" || {
	echo "Run this script from the top-level directory in the"
	echo "repository"
	exit 1
}

usage() {
	echo "usage: $0 <NAME> <HEADLINE>"
	echo ""
	echo "This will open \$EDITOR in order to fill in the body content"
	echo ""
}

test -n "${EDITOR-}" || {
	usage
	exit 1
}

NAME=${1-}
test -n "${NAME}" || {
	usage
	exit 1
}
shift 1
HEADLINE=$(echo "$@" | xargs)
test -n "${HEADLINE}" || {
	usage
	exit 1
}

grep -q "<a name=\"${NAME}\"" posted_news/*.body.html && {
	echo "NAME[${NAME}] is not unique, choose another name"
	grep "<a name=\"${NAME}\"" posted_news/*.body.html
	exit 1
}

nd=$(git diff -p --stat HEAD | wc -l)
test x"${nd}" = x"0" || {
	echo "You have uncommited changes your working tree"
	git status
	exit 1
}

trap_handler() {
	echo ""
	echo "ERROR: cleaning up"
	echo ""

	for f in ${CLEANUP_FILES}; do
		echo "Removing file[${f}]"
		test -f "${f}" && {
			rm "${f}" || {
				echo "failed to remove ${f}"
			}
		}
	done

	test -n "${CLEANUP_RESET_COMMIT}" && {
		echo "Reverting to commit[${CLEANUP_RESET_COMMIT}]"
		git reset "${CLEANUP_RESET_COMMIT}"
	}
}

CLEANUP_FILES=""
trap trap_handler INT QUIT TERM EXIT

href="#${NAME}"
utcdate=$(date --utc +"%d %B %Y")
utctime=$(date --utc +"%Y%m%d-%H%M%S")

headlinefile="posted_news/${utctime}.${NAME}.headline.html"
bodyfile="posted_news/${utctime}.${NAME}.body.html"
snipfile=""
echo "generating ${headlinefile}"
CLEANUP_FILES="${CLEANUP_FILES} ${headlinefile}"
{
	echo "<!-- BEGIN: ${headlinefile} -->"
	echo "<li> ${utcdate} <a href=\"${href}\">${HEADLINE}</a></li>"
	echo "<!-- END: ${headlinefile} -->"
} > ${headlinefile}

echo "generating ${bodyfile}"
CLEANUP_FILES="${CLEANUP_FILES} ${bodyfile}"
{
	echo "<!-- BEGIN: ${bodyfile} -->"
	echo "<h5><a name=\"${NAME}\">${utcdate}</a></h5>"
	echo "<p class=headline>${HEADLINE}</p>"
	echo "<p>"
	echo "<!-- TODO: add your content here -->"
	echo "</p>"
	echo "<p>"
	echo "<!-- TODO: add more content here -->"
	echo "</p>"
	echo "<!-- SNIP -->"
	echo "<!-- TODO: remove the above line or move it to where you want the front-page preview to end, also remove this comment -->"
	echo "<!-- END: ${bodyfile} -->"
} > ${bodyfile}

LC_ALL="${SAVE_LC_ALL}" \
LANG="${SAVE_LANG}" \
LANGUAGE="${SAVE_LANGUAGE}" \
${EDITOR} ${bodyfile}

if grep -q "<!-- SNIP -->" $bodyfile; then
	snipfile="posted_news/${utctime}.${NAME}.snip.html"
	echo "generating ${snipfile}"
	CLEANUP_FILES="${CLEANUP_FILES} ${snipfile}"
	{
		echo "<!-- BEGIN: ${snipfile} -->"
		while read -r line || [[ -n "$line" ]]; do
			if [[ $line == *"<!-- BEGIN"* ]]; then
				continue
			elif [[ $line == *"<!-- SNIP -->"* || $line == *"<!-- END"* ]]; then
				echo ${line%%<!--*}"<p><a href=\"/samba/latest_news.html#${NAME}\">(Read more)</a></p>"
				break
			else
				echo $line
			fi
		done <"${bodyfile}"
		echo "<!-- END: ${snipfile} -->"
	} > ${snipfile}
fi

CLEANUP_FILES=""
CLEANUP_RESET_COMMIT="HEAD"
git add ${headlinefile} ${bodyfile} ${snipfile}
git commit --signoff --message "NEWS[${NAME}]: ${HEADLINE}"
CLEANUP_RESET_COMMIT="HEAD^"

echo ""
echo "Once you have pushed the commit a cronjob updates"
echo "the webserver content every 5-10 mins."
echo ""
echo "Please verify the commit carefully before pushing:"
echo ""
echo "  git show -p --stat HEAD"
echo "  git push ..."
echo ""

trap - INT QUIT TERM EXIT
exit 0
