#!/bin/sh
#

SAVE_LC_ALL="${LC_ALL}"
SAVE_LANG="${LANG}"
SAVE_LANGUAGE="${LANGUAGE}"

LC_ALL=C
export LC_ALL
LANG=C
export LANG
LANGUAGE=C
export LANGUAGE

set -u
set -e
umask 0022

test -d ".git" || {
	echo "Run this script from the top-level directory in the"
	echo "repository"
	exit 1
}

usage() {
	echo "usage: $0 <NAME> <PATCHFILE>"
	echo "$@"
}

NAME=${1-}
test -n "${NAME}" || {
	usage name
	exit 1
}
PATCHFILE=${2-}
test -n "${PATCHFILE}" || {
	usage patchfile
	exit 1
}

test -f "${PATCHFILE}" || {
	usage file
	exit 1
}

grep -q "<a name=\"${NAME}\"" "${PATCHFILE}" || {
	echo "NAME[${NAME}] does not match content of PATCHFILE[${PATCHFILE}]"
	exit 1
}

grep -q "<a name=\"${NAME}\"" posted_news/*.body.html && {
	echo "NAME[${NAME}] is not unique, choose another name"
	grep "<a name=\"${NAME}\"" posted_news/*.body.html
	exit 1
}

nd=$(git diff -p --stat HEAD | wc -l)
test x"${nd}" = x"0" || {
	echo "You have uncommited changes your working tree"
	git status
	exit 1
}

trap_handler() {
	echo ""
	echo "ERROR: cleaning up"
	echo ""

	for f in ${CLEANUP_FILES}; do
		echo "Removing file[${f}]"
		test -f "${f}" && {
			rm "${f}" || {
				echo "failed to remove ${f}"
			}
		}
	done

	test -n "${CLEANUP_RESET_COMMIT}" && {
		echo "Reverting to commit[${CLEANUP_RESET_COMMIT}]"
		git reset "${CLEANUP_RESET_COMMIT}"
	}
}

CLEANUP_FILES=""
trap trap_handler INT QUIT TERM EXIT

utcdate=$(date --utc +"%d %B %Y")
utctime=$(date --utc +"%Y%m%d-%H%M%S")

CLEANUP_FILES=""
CLEANUP_RESET_COMMIT="HEAD"
cat "${PATCHFILE}" | \
	sed -e "s!@UTCTIME@!${utctime}!g" -e "s!@UTCDATE@!${utcdate}!g" | \
	git am --ignore-whitespace
CLEANUP_RESET_COMMIT="HEAD^"

echo ""
echo "Once you have pushed the commit a cronjob updates"
echo "the webserver content every 5-10 mins."
echo ""
echo "Please verify the commit carefully before pushing:"
echo ""
echo "  git show -p --stat HEAD"
echo "  git push ..."
echo ""

trap - INT QUIT TERM EXIT
exit 0
